$(document).ready(function() {
	/*----------------------------------------------------*/
	/* MOBILE DETECT FUNCTION
	/*----------------------------------------------------*/
	var isMobile = {
	    Android: function() {
	        return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function() {
	        return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function() {
	        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function() {
	        return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function() {
	        return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function() {
	        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};

	/*----------------------------------------------------*/
	// PARALLAX CALLING
	/*----------------------------------------------------*/
	$(window).bind('load', function () {
		parallaxInit();
	});
	function parallaxInit() {
		testMobile = isMobile.any();

		if (testMobile == null)
		{
			$('.parallax .bg1').addClass("bg-fixed").parallax("50%", 0.5);
		}
	}
	parallaxInit();

	// --- search panel
	var searchBtn = $('#search').children('.searchBtn'),
		searchPanel = searchBtn.next(),
		searchP = searchBtn.parent();
	searchBtn.click(function(e){
		e.preventDefault();
		var _t = $(this);
		if(!_t.hasClass('active')) {
			_t.addClass('active')
			//.find('span')
			//.removeClass('icon-search icon-white')
			//.addClass('icon-remove');
			searchPanel.show();
		} else {
			_t.removeClass('active')
			//.find('span')
			//.addClass('icon-search icon-white')
			//.removeClass('icon-remove');
			searchPanel.hide();
		}
	}); // searchBtn.click //
	$(document).click(function(){
		searchBtn.removeClass('active')
			//.find('span')
			//.addClass('icon-search icon-white')
			//.removeClass('icon-remove');
		searchPanel.hide(0);
	});
	searchP.click(function(event){
		event.stopPropagation();
	});
	// --- end search panel



}); //
$(window).load(function() {
	//



}); //