<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class CallCenterModel extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->model("Lekerdezes");
    }

    
    public function get_company_type(){
        $query = $this->db->query("SELECT name FROM companytype");
        $array[] = "";
        foreach($query->result() as $row)
            $array[] = $row->name;
        
        return $array;
    }
}
