<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lekerdezes extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	public function record_count($id) {
		
		$this->db->where('kategoria',$id);
        return $this->db->count_all_results("hirek");
    }
 
    public function fetch_hirek($limit, $start, $id) {
		$this->db->where('kategoria',$id);
		$this->db->where( 'statusz !=',0);
		$this->db->order_by("id","DESC");
        $this->db->limit($limit, $start);
		
        $query = $this->db->get("hirek");
 
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
   }
	function bolcsessegek($option)
	{
		
		return "";
		
	}

    function hir_kategoriak($url=null) {
        //create query to connect user login database
        $this->db->select('*');
        $this->db->from('hirek_kategoria');
		if($url != ""){
		$this->db->where('url',$url);	
		}
        
        return $this->db->get();
           

    }
	
	function donate() {
        $query = $this->db->query("SELECT * FROM donate order by osszeg  DESC");
        return $query;

    }

    function oldal($url) {
        //create query to connect user login database
        $this->db->select('*');
        $this->db->from('oldalak');
        $this->db->where('url',$url);

        $query = $this->db->get();


        return $query->row();
    }
	
    function kategoria_hirek($url)
	{
		$option = array("url"=>$url);
		$kat = $this->hir_kategoriak($option);
		$kat = $kat->row();
		$id = @$kat->id;
		if(!empty($id)){
		$this->db->select('*');
        $this->db->from('hirek');
        $this->db->where('kategoria',$id);

        $query = $this->db->get();

		return $query;
		}else{  return false; }
		
	}
	

    function megjelenes($option)
    {
        $query = $this->db->query("SELECT * FROM megjelenes ".$option);
        return $query;

    }
	
	function hirek($url)
    {
        $query = $this->db->query("SELECT *,(select nev FROM hirek_kategoria WHERE id= h.kategoria) as hireKategoria FROM hirek h WHERE url = '$url'  ");
        return $query;

    }
	function hirek_option($option)
    {
        $query = $this->db->query("SELECT * FROM hirek ".$option);

        return $query;

    }

    function beallitasok($option = null)
    {
        $query = $this->db->query("SELECT * FROM beallitasok WHERE id=1");
        $adatok = $query->row();
        return $adatok;
    }
    function felhasznalok()
    {
        $query = $this->db->query("SELECT * FROM users ");
        $adatok = $query->row();
        return $adatok;
    }

    function slider($lang)
    {
        $query = $this->db->query("SELECT * FROM slider WHERE nyelv = '$lang' ");
        return $query;
    }

    function termek_lekerdezes($option)
    {
        $query = $this->db->query("SELECT * FROM termekek ".$option);

        return $query;
    }

    function tulajdonsag_kat($option)
    {
        $tomb[0] = "";
        $query = $this->db->query("SELECT * FROM tulajdonsag_kat ".$option);

        foreach($query->result() as $sor)
        {
            $tomb[$sor->id] = $sor->nev;
        }

        return $tomb;
    }

    //Főmenü
    function primari($lang){

        $query = $this->db->query("SELECT * FROM oldalak WHERE nyelv='$lang' AND statusz != 0 order by sorrend");

        //$result = mysql_query("")or die(mysql_error());



        //Oldalakat tömbbe rakjuk
        foreach($query->result() as $row){

            @$menutomb[] = array(
                'id' => $row->id,
                'nev' => $row->nev,
                'url' => $row->url,
                'szulo' => $row->szulo,
                'cim' => $row->cim,
                'sorrend' => $row->sorrend,
                'statusz' => $row->statusz,
                'nyelv' => $row->nyelv
            );
        }




        foreach($menutomb  as $sor){

            //Ha szülő az illető
            if($sor['szulo'] == 0){
                $gyerekek = "";

                foreach($menutomb as $gyerek){

                    //Ha az adott aloldal a szulóhöz tartozik
                    if($gyerek['szulo'] == $sor['id']){
                        @$gyerekek[] = array(
                            id => $gyerek['id'],
                            nev => $gyerek['nev'],
                            url => $gyerek['url'],
                            szulo => $gyerek['szulo'],
                            cim => $gyerek['cim'],
                            statusz => $gyerek['statusz'],
                            nyelv => $gyerek['nyelv']
                        );
                    }
                }

                @$szuloktomb[] = array(
                    id => $sor['id'],
                    nev => $sor['nev'],
                    url => $sor['url'],
                    szulo => $sor['szulo'],
                    cim => $sor['cim'],
                    statusz => $sor['statusz'],
                    gyerekek => $gyerekek,
                    nyelv => $sor['nyelv']
                );

            }

        }

        return $szuloktomb;
    }
}