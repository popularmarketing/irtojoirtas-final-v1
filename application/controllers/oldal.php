<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oldal extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");


        $this->load->model("Lekerdezes");
        $this->load->model("Alapfunction");
        $this->load->helper("url");
        $this->load->library("pagination");

    }

    public function index()
    {
        $nyelv = $this->input->get('lang', TRUE);	
		$data['nyelv'] = $this->Alapfunction->nyelv($nyelv);
		$data['oldal'] = $this->Lekerdezes->oldal('index');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$data['slider'] = $this->Lekerdezes->slider($data['nyelv']);
        $this->load->view("site/fooldal",$data);
    }

    public function szolgaltatasok()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('szolgaltatasok');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
        $this->load->view("site/szolgaltatasok",$data);
    }
	public function hangyairtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('hangyairtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
        $this->load->view("site/hangyairtas",$data);
    }
	public function patkanyirtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('patkanyirtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
        $this->load->view("site/patkanyirtas",$data);
    }
	public function csotanyirtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('csotanyirtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
        $this->load->view("site/csotanyirtas",$data);
    }
	public function darazsirtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('darazsirtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/darazsirtas",$data);
    }
	public function agyipoloskairtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('agyipoloskairtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();  
        $this->load->view("site/agyipoloskairtas",$data);
    }
	public function rovarirtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('rovarirtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
        $this->load->view("site/rovarirtas",$data);
    }
	public function ragcsaloirtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('ragcsaloirtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/ragcsaloirtas",$data);
    }
	public function gyomirtas()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('gyomirtas');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/gyomirtas",$data);
    }
	public function referenciak()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('referenciak');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/referenciak",$data);
    }
	public function elerhetoseg()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('elerhetoseg');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/elerhetoseg",$data);
    }
	public function error()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('404');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/404",$data);
    }
	public function cegunkrol()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('cegunkrol');
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/cegunkrol",$data);
    }
	public function galeria()
    {
        $nyelv = $this->input->get('lang', TRUE);
        $data['oldal'] = $this->Lekerdezes->oldal('galeria');
		$data['kepek'] = $this->db->query("SELECT * FROM galeria WHERE active = 2");
		$data['bealitasok'] = $this->Lekerdezes->beallitasok();
		$this->load->view("site/galeria",$data);
    }
}

