
<body class="dark-theme">
<div class="header navbar navbar-inverse box-shadow navbar-fixed-top">
    <div class="navbar-inner">
        <div class="header-seperation">
            <ul class="nav navbar-nav">
                <li class="sidebar-toggle-box"> <a href="#"><i class="fa fa-bars"></i></a> </li>
                <li> <a href="<?php echo  base_url("admin")?>"><strong><?php echo $oldalnev?></strong></a> </li>
                <li class="hidden-xs"> <a href="<?php echo base_url("admin/logout");?>" target="_BLANK"><i class="fa fa-angle-double-left"></i>Kijelentkezés</a> </li>
            
        </div><!--/header-seperation-->
    </div><!--/navbar-inner-->
</div><!--/header-->


<div class="page-container">

    <div class="nav-collapse top-margin fixed box-shadow2 hidden-xs" id="sidebar">
        <div class="leftside-navigation" style="padding-top:10px;">

        <ul id="nav-accordion" class="sidebar-menu">
            <li> <a href="<?php echo base_url("admin/oldalak");?>" class="active"> <i class="fa fa-book"></i> <span>Oldalak</span> </a> </li>
			<li> <a href="<?php echo base_url("admin/slider");?>" class="active"> <i class="fa fa-dashboard"></i> <span>Slider</span> </a> </li>
			<li> <a href="<?php echo base_url("admin/galeria");?>" class="active"> <i class="fa fa-dashboard"></i> <span>Galléria</span> </a> </li>
            <li> <a href="<?php echo base_url("admin/beallitasok");?>" class="active"> <i class="fa fa-dashboard"></i> <span>Beállítások</span> </a> </li>
        </ul><!--/nav-accordion sidebar-menu-->
    </div><!--/leftside-navigation-->
</div><!--/sidebar-->

<?php $uri = $this->uri->segment(2);?>
<div id="main-content">
<div class="page-content">
<div class="row">
    <div class="col-md-12">
        <h2><i class="fa fa-dashboard"></i> <?php if(!empty($oldaltitle)){ echo $oldaltitle;}else{ echo "Főoldal"; }?></h2>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?php if(!empty($leiras)){echo $leiras;} ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12"> 
        <?php if(!empty($output)) echo $output; ?>
        <?php switch($uri){
            case "shownotes":
                include_once("site/shownotes.php");
                break;
        } ?>
    </div>
</div>

</div>
</div>
</div>


