<!DOCTYPE html>

<html>
    <head>
		<base href="<?php echo base_url(); ?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="<?php print_r($oldal->keywords); ?>" />
        <meta name="description" content="<?php print_r($oldal->description); ?>" />
        <meta name="author" content="">
        <title>Irt&oacute; J&oacute; Irt&aacute;s | <?php print_r($oldal->nev); ?></title>
        
        <!-- Stylesheets -->
        <link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/font-awesome.css" rel="stylesheet">
		<link href="css/camera.css" rel="stylesheet">
		<link href="css/touchTouch.css" rel="stylesheet">
		<link href="css/isotope.css" rel="stylesheet">
		<link href="css/style.css" rel="stylesheet">

		
		<!-- Scripts -->			
		
		<script src="js/jquery.js"></script>
		<script src="js/jquery-migrate-1.2.1.min.js"></script>
		<script src="js/jquery.easing.1.3.js"></script>
		<script src="js/superfish.js"></script>
		
		<script src="js/camera.js"></script>
		<script src="js/jquery.mobile.customized.min.js"></script>

		<script src="js/jquery.parallax-1.1.3.resize.js"></script>

		<script src="js/SmoothScroll.js"></script>

		<script src="js/jquery.ui.totop.js"></script>

		<script src="js/jquery.caroufredsel.js"></script>
		<script src="js/jquery.touchSwipe.min.js"></script>
		
		<script src="js/cform.js"></script>
		<script src="js/touchTouch.jquery.js"></script>
		<script src="js/jquery.isotope.min.js"></script>

		<script src="js/script.js"></script>
		
		<?php print_r($oldal->header_custom_code); ?> 
    </head>
    <body>
		<div id="main">