<div id="main">
<div class="top1_wrapper">
  <div class="container">
    <div class="top1 clearfix">
      <header>
        <div class="logo_wrapper">
          <a href="<?php echo base_url() ?>" class="logo">
            <img src="assets/uploads/files/<?php print_r($bealitasok->logo); ?>" alt="" class="img-responsive">
          </a>
        </div>
		</header>
		<div class="top2_wrapper clearfix">
        <div class="call1_wrapper">
          <div class="call1">
            <a href="elerhetoseg">
              <div class="txt1">Vezetékes számunk:</div>
              <div class="txt2"><?php print_r($bealitasok->vezetekes); ?></div>
            </a>
          </div>
        </div>
        <div class="chat1_wrapper">
          <div class="call1">
            <a href="elerhetoseg">
              <div class="txt1">Mobil számunk:</div>
              <div class="txt2"><?php print_r($bealitasok->mobil); ?></div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="top3_wrapper">
  <div class="container">
    <div class="top3 clearfix">
		<div class="menu_wrapper">
				<div class="navbar navbar_ navbar-default">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <div class="navbar-collapse navbar-collapse_ collapse">
					<ul class="nav navbar-nav sf-menu clearfix">
					  <li class="sub-menu sub-menu-1"><a href="<?php echo base_url()?>cegunkrol">Cégünkről<em></em></a>
						<ul>
							<li><a href="<?php echo base_url()?>galeria">Galéria</a></li>
						</ul>
						</li>
					  <li class="sub-menu sub-menu-1"><a href="<?php echo base_url()?>szolgaltatasok">Szolgáltatások<em></em></a>
						<ul>
						  <li><a href="<?php echo base_url()?>hangyairtas">Hangyairtás</a></li>
						  <li><a href="<?php echo base_url()?>patkanyirtas">Patkányirtás</a></li>
						  <li><a href="<?php echo base_url()?>csotanyirtas">Csótányirtás</a></li>
						  <li><a href="<?php echo base_url()?>darazsirtas">Darázsirtás</a></li>
						  <li><a href="<?php echo base_url()?>agyipoloskairtas">Ágyi poloska irtás</a></li>
						</ul>
					  </li>
					  <li><a href="<?php echo base_url()?>ragcsaloirtas">Rágcsálóirtás</a></li>
					  <li><a href="<?php echo base_url()?>rovarirtas">Rovarirtás</a></li>
					  <li><a href="<?php echo base_url()?>gyomirtas">Gyomirtás</a></li>
					  <li><a href="<?php echo base_url()?>referenciak">Referenciák</a></li>
					  <li><a href="<?php echo base_url()?>elerhetoseg">Elérhetőségünk</a></li>
					</ul>
				</div>
			</div>
		</div>
    </div>
  </div>
</div>