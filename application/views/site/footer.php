	<div id="parallax1" class="parallax">
	  <div class="bg1 parallax-bg"></div>
	  <div class="parallax-content">
		<div class="container">
		  <div class="row">
			<div class="col-sm-4">
			  <div class="thumb2">
				<div class="thumbnail clearfix">
				  <a href="ragcsaloirtas">
					<figure class="">
					  <img src="images/banner1.png" alt="" class="img-responsive">
					</figure>
					<div class="caption">
					  <div class="txt1">Rágcsálóirtás</div>
					  <div class="txt2">Az egészségügyi rendeletek a gyártókat, illetve az élelmiszerforgalmazókat is kötelezik a rendszeres rágcsálóirtás elvégzésére.</div>
					</div>
				  </a>
				</div>
			  </div>
			</div>
			<div class="col-sm-4">
			  <div class="thumb2">
				<div class="thumbnail clearfix">
				  <a href="rovarirtas">
					<figure class="">
					  <img src="images/banner2.png" alt="" class="img-responsive">
					</figure>
					<div class="caption">
					  <div class="txt1">Rovarirtás</div>
					  <div class="txt2">A korszerű technológiával és szakértelemmel teljes mértékben garantáljuk az eredményes munkavégzést Önnek!</div>
					</div>
				  </a>
				</div>
			  </div>
			</div>
			<div class="col-sm-4">
			  <div class="thumb2">
				<div class="thumbnail clearfix">
				  <a href="gyomirtas">
					<figure class="">
					  <img src="images/banner3.png" alt="" class="img-responsive">
					</figure>
					<div class="caption">
					  <div class="txt1">Gyomirtás</div>
					  <div class="txt2">Válasszák Önök is takarékos, hatékony, környezetkímélő szolgáltatásunkat!</div>
					</div>
				  </a>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
	<div id="content2">
	  <div class="container">
		<div class="row">
		  <div class="col-sm-4">

			<h3>Partnereink</h3>

			<ul class="ul1">
			  <li><a href="http://madarkontroll.hu/" target="_blank">MadárKontroll</a></li>
			  <li><a href="http://tanusitunk.hu/" target="_blank">Tanusítunk.hu</a></li>
			  <li><a href="http://greennovate.hu/" target="_blank">Greennovate</a></li>
			</ul>

		  </div>
		  <div class="col-sm-4"></div>
		  <div class="col-sm-4">

			<h3 style="text-align: center;">Megbízható cég</h3>

			<div class="thumb3">
			  <div class="thumbnail clearfix">
				<div style="margin: 0 auto;">
					<a style="margin: 0 auto;" href="http://www.megbizhatoceg.com/show,4975,rtjrts-kft-budapest" target="_blank">
						<img style="margin: 0 auto;" src="images/megbizhato-ceg.jpg" alt="Irtójóírtás.hu megbízható cég tanúsítvány">
					</a>
				</div>
			  </div>
			</div>

		  </div>
		</div>
	  </div>
	</div>
	<div class="bot1_wrapper">
	  <div class="container">
		<div class="bot1 clearfix">
		  <footer>© 2016 - Weboldalt készítette <a href="http://popularmarketing.hu" target="_blank">Popular Marketing</a> - Minden jog fenntartva</footer>
		</div>
	  </div>
	</div>
</div>
<script src="js/bootstrap.min.js"></script>
</body>
</html>