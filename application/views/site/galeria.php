<?php include("header.php"); ?>
<?php include("primari.php") ?>
<div class="page_slide">
  <div class="page_slide_img"><img src="images/page_slide3.jpg" alt=""></div>
  <div class="page_title_wrapper">
    <div class="container">
      <div class="page_title"><?php print_r($oldal->nev); ?></div>
    </div>
  </div>
</div>
<div id="content">
  <div class="container">
	<div class="row">
		<div class="col-sm-12">
		  <div class="isotope-box">
			<div id="container" class="clearfix">
			  <ul class="thumbnails" id="isotope-items">
				  <?php  foreach($kepek->result() as $row){ ?> 
							  <li class="element isotope-filter1">
							  <div class="thumb-isotope">
								<div class="thumbnail clearfix">
								  <a href="<?php echo base_url("/assets/uploads/files/".$row->file)?>">
									<figure>
									  <img src="<?php echo base_url("/assets/uploads/files/".$row->file)?>" alt=""><em></em>
									</figure>
								  </a>
								</div>
							  </div>
							</li>
						<?php }?>
			  </ul>
			</div>
		  </div>
		</div>
		</div>
	</div>
</div>
 <?php include("footer.php");?>