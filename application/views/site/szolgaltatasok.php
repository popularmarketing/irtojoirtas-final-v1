<?php include("header.php"); ?>
<?php include("primari.php") ?>
<div class="page_slide">
  <div class="page_slide_img"><img src="images/page_slide3.jpg" alt=""></div>
  <div class="page_title_wrapper">
    <div class="container">
      <div class="page_title">Szolgáltatások</div>
    </div>
  </div>
</div>
<div id="content">
  <div class="container">
	<div class="row">
      <div class="col-sm-9">
		<?php print_r($oldal->tartalom); ?>
      </div>
      <div class="col-sm-3">
        <?php include("oldalbar.php") ?>
      </div>
    </div>
  </div>
</div>
<?php include("footer.php"); ?>