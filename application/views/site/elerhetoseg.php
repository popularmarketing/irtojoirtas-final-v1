<?php include("header.php"); ?>
<?php include("primari.php") ?>
<div class="page_slide">
  <div class="page_slide_img"><img src="images/page_slide3.jpg" alt=""></div>
  <div class="page_title_wrapper">
    <div class="container">
      <div class="page_title"><?php print_r($oldal->nev); ?></div>
    </div>
  </div>
</div>
<div id="content">
  <div class="container">
	<div class="row">
	<div class="col-sm-1"></div>
	<div class="col-sm-4">
		<?php print_r($oldal->tartalom); ?>
	</div>
	<div class="col-sm-6">

        <h2>Telephelyünk</h2>

        <figure class="google_map">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2700.4665922015465!2d19.18365971578193!3d47.402839510024336!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4741e9f73b3ba491%3A0x522fd2c35d44f905!2zSXJ0w7Nqw7PDrXJ0w6FzIEtmdA!5e0!3m2!1shu!2shu!4v1456571008060" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </figure>
      </div>
	  <div class="col-sm-1"></div>
  </div></div>
</div>
<?php include("footer.php"); ?>