<?php include("header.php"); ?>
<?php include("primari.php") ?>
<?php include("slider.php") ?>
<div class="splash_wrapper">
  <div class="container">
    <div class="title1"><?php print_r("Teljeskörű rovar, rágcsáló és gyomirtás."); ?></div>
    <div class="title2"><?php print_r("Mi meg tudjuk védeni Önt!"); ?></div>

    <div class="row">
	<div class="col-sm-1"></div>
      <div class="col-sm-2">
        <div class="thumb1">
          <div class="thumbnail clearfix">
            <a href="darazsirtas">
              <figure class="">
                <img src="images/pest1.png" alt="" class="img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Darazsak</div>
				<div class="txt2">A darazsak társas kolóniákban élnek, egyéves családokban.</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="thumb1">
          <div class="thumbnail clearfix">
            <a href="hangyairtas">
              <figure class="">
                <img src="images/pest2.png" alt="" class="img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Hangyák</div>
				<div class="txt2">A hangyák társas családokba szerveződve, ún. bolyokban élnek.</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="thumb1">
          <div class="thumbnail clearfix">
            <a href="csotanyirtas">
              <figure class="">
                <img src="images/pest3.png" alt="" class="img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Csótányok</div>
				<div class="txt2">Megfelelő életfeltételek mellett elszaporodhatnak gyakorlatilag bárhol.</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="thumb1">
          <div class="thumbnail clearfix">
            <a href="agyipoloskairtas">
              <figure class="">
                <img src="images/pest4.png" alt="" class="img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Ágyi poloskák</div>
				<div class="txt2">Az ágyi poloska a fénykerülő, vérszívó rovarok közé tartozik.</div>
              </div>
            </a>
          </div>
        </div>
      </div>
      <div class="col-sm-2">
        <div class="thumb1">
          <div class="thumbnail clearfix">
            <a href="patkanyirtas">
              <figure class="">
                <img src="images/pest6.png" alt="" class="img-responsive">
              </figure>
              <div class="caption">
                <div class="txt1">Patkányok</div>
				<div class="txt2">Társas élőlény, kisebb-nagyobb hordákban él, éjszakai életmódot folytat.</div>
              </div>
            </a>
          </div>
        </div>
      </div>
	  <div class="col-sm-1"></div>
    </div>

    <div class="splash_bot">
      <div class="link_wrapper"><a href="elerhetoseg" class="btn-default btn1">Hívjon minket!</a></div>
    </div>
  </div>
</div>
<div id="content">
  <div class="container">
	<div class="row">
		<div class="col-sm-6 col-sm-push-6">
			<div class="box1_wrapper">
			  <div class="box1">
				<div class="img"><img src="images/mouse1.jpg" alt="" class="img-responsive"></div>
				<div class="box1_inner">
				  <div class="title3">Tudjuk, hogy a rovarirtás, rágcsálóirtás feladatait nem bölcs dolog halogatni. Ezért, a megrendelés leadását követően két napon belül kiszállunk az ország bármely pontjára, és elvégezzük a kártevő-mentesítést. Hívjon minket vagy érdeklődjön e-mailben árainkról.</div>
				</div>
			  </div>
			</div>
			
		</div>
		<div class="col-sm-6 col-sm-pull-6">
			<?php print_r($oldal->tartalom); ?>
		</div>
	</div>
  </div>
</div>
<?php include("footer.php"); ?>