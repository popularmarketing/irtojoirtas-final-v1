-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Gép: localhost:3306
-- Létrehozás ideje: 2016. Feb 27. 03:47
-- Kiszolgáló verziója: 5.6.26
-- PHP verzió: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `alapwebsite`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `beallitasok`
--

CREATE TABLE IF NOT EXISTS `beallitasok` (
  `id` int(11) NOT NULL,
  `oldalnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `logo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `favicon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `vezetekes` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fax` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `adoszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telephelycim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `uzletcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` text COLLATE utf8_hungarian_ci NOT NULL,
  `adminemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyilvanosemail` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitva` int(1) NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `fooldal_description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `google_analytics` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `beallitasok`
--

INSERT INTO `beallitasok` (`id`, `oldalnev`, `logo`, `favicon`, `mobil`, `vezetekes`, `fax`, `adoszam`, `telephelycim`, `uzletcim`, `nyitvatartas`, `adminemail`, `nyilvanosemail`, `nyitva`, `nyelv`, `fooldal_title`, `fooldal_keywords`, `fooldal_description`, `google_analytics`) VALUES
(1, 'Irtó Jó Irtás', '151de-logo.png', '', '06 20/587-89-48', '06 1/630-47-20', '06 1/630-47-20', '', '', '', '', '', 'aron98@gmail.com', 1, 'hu', 'Irtó Jó Irtás | Cégünkről', '', '', ''),
(2, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'de', '', '', '', ''),
(3, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'en', '', '', '', ''),
(4, '', '', '', '', '', '', '', '', '', '', '', '', 0, 'cz', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `companytype`
--

CREATE TABLE IF NOT EXISTS `companytype` (
  `id` int(11) NOT NULL,
  `name` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `link5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `companytype`
--

INSERT INTO `companytype` (`id`, `name`, `link1`, `link2`, `link3`, `link4`, `link5`) VALUES
(1, 'Étterem', '', '', '', '', ''),
(2, 'Fodrászat', '', '', '', '', ''),
(3, 'Cukrászda/Fagyizó', '', '', '', '', ''),
(4, 'Fitnesz / Gym / Edzőterem', '', '', '', '', ''),
(5, 'Tetóválószalon', '', '', '', '', ''),
(6, 'Plasztikai Sebészet', '', '', '', '', ''),
(7, 'Esküvői szalon', '', '', '', '', ''),
(8, 'Sexshop', '', '', '', '', ''),
(9, 'Ipari cikk', '', '', '', '', ''),
(10, 'Kozmetika', '', '', '', '', ''),
(11, 'Ruhazati boltok', '', '', '', '', ''),
(12, 'Utazasi iroda', '', '', '', '', ''),
(13, 'Konyveloi iroda', '', '', '', '', ''),
(14, 'Nyelviskola', '', '', '', '', ''),
(15, 'Fogászat', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `dokumentumok`
--

CREATE TABLE IF NOT EXISTS `dokumentumok` (
  `dokumentumokid` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
  `id` int(11) NOT NULL,
  `image_url` varchar(256) NOT NULL,
  `file` varchar(256) NOT NULL,
  `nev` varchar(256) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `galeria`
--

INSERT INTO `galeria` (`id`, `image_url`, `file`, `nev`, `active`) VALUES
(3, '', 'acf6e-cegauto.png', '', 2),
(4, '', 'c3a79-rovar-es-ragcsaloirtas.png', '', 2),
(5, '', '', '', 0),
(6, '', '7c9ae-home01.jpg', '', 2),
(7, '', '', '', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyarto`
--

CREATE TABLE IF NOT EXISTS `gyarto` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `gyik`
--

CREATE TABLE IF NOT EXISTS `gyik` (
  `cim` varchar(1000) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(255) NOT NULL,
  `tag` varchar(255) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek`
--

CREATE TABLE IF NOT EXISTS `hirek` (
  `id` int(11) NOT NULL,
  `url` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` date NOT NULL,
  `statusz` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `fokep` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tag` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `videoid` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `user` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `hirek_kategoria`
--

CREATE TABLE IF NOT EXISTS `hirek_kategoria` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `fokep` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoria`
--

CREATE TABLE IF NOT EXISTS `kategoria` (
  `kategoriaid` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `icon` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `markericon` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kategoriak`
--

CREATE TABLE IF NOT EXISTS `kategoriak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `kerdezzfelelek`
--

CREATE TABLE IF NOT EXISTS `kerdezzfelelek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) CHARACTER SET utf32 COLLATE utf32_hungarian_ci NOT NULL,
  `email` varchar(256) NOT NULL,
  `kerdes` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `kitol` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `marka`
--

CREATE TABLE IF NOT EXISTS `marka` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `megjelenes`
--

CREATE TABLE IF NOT EXISTS `megjelenes` (
  `megjelenesid` int(11) NOT NULL,
  `ugyfelszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cegnev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `iranyitoszam` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `varos` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(400) COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg2` text COLLATE utf8_hungarian_ci NOT NULL,
  `youtubeid` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep1` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep2` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep3` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep4` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kep5` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `telefonszam` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `mobil` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `webcim` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyitvatartas` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `letrehozva` datetime NOT NULL,
  `lejarat` datetime NOT NULL,
  `gpsx` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `gpsy` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `nyelvi_forditasok`
--

CREATE TABLE IF NOT EXISTS `nyelvi_forditasok` (
  `id` int(11) NOT NULL,
  `azonosito` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `oldalak`
--

CREATE TABLE IF NOT EXISTS `oldalak` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(11) NOT NULL,
  `cim` varchar(560) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(10) NOT NULL,
  `statusz` int(1) NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `oldalak`
--

INSERT INTO `oldalak` (`id`, `nev`, `url`, `tartalom`, `szulo`, `cim`, `sorrend`, `statusz`, `fokep`, `title`, `keywords`, `description`, `header_custom_code`, `nyelv`) VALUES
(2, 'Kezdőlap', 'index', '<div>\r\n	<h2>\r\n		Tev&eacute;kenys&eacute;g&uuml;nk</h2>\r\n	<h4>\r\n		Professzion&aacute;lis rovarirt&aacute;s &eacute;s r&aacute;gcs&aacute;l&oacute;irt&aacute;s</h4>\r\n	<p>\r\n		Az Irt&oacute;j&oacute;irt&aacute;s Kft. Magyarorsz&aacute;g teljes ter&uuml;let&eacute;n v&aacute;llalja a gyom- &eacute;s k&aacute;rtevőirt&aacute;s elv&eacute;gz&eacute;s&eacute;t a legkorszerűbb m&oacute;dszerekkel, eszk&ouml;z&ouml;kkel. Budapesten &eacute;s Pest megy&eacute;ben nem sz&aacute;m&iacute;tunk fel kisz&aacute;ll&aacute;si d&iacute;jat &eacute;s egy&eacute;b k&ouml;lts&eacute;geket, &iacute;gy m&eacute;g kedvezőbb &aacute;ron rendelheti meg szolg&aacute;ltat&aacute;sainkat!</p>\r\n	<ul>\r\n		<li>\r\n			Gyomirt&aacute;s: k&ouml;rnyezetk&iacute;m&eacute;lő n&ouml;v&eacute;nyv&eacute;delem, gyomok &eacute;s allerg&eacute;n n&ouml;v&eacute;nyek irt&aacute;sa</li>\r\n		<li>\r\n			Rovarirt&aacute;s: cs&oacute;t&aacute;nyirt&aacute;s, hangyairt&aacute;s, dar&aacute;zsirt&aacute;s, bolha- &eacute;s kullancsirt&aacute;s, valamint &aacute;gyi poloska irt&aacute;sa t&ouml;bbf&eacute;le rovarirt&aacute;si m&oacute;dszerrel, technik&aacute;val</li>\r\n		<li>\r\n			R&aacute;gcs&aacute;l&oacute;irt&aacute;s: eg&eacute;rirt&aacute;s, patk&aacute;nyirt&aacute;s csal&eacute;tkes m&oacute;dszerekkel, pocok- &eacute;s vakond irt&aacute;sa hat&eacute;konyan</li>\r\n	</ul>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '<script>\r\n$(document).ready(function() {\r\n  //\r\n\r\n  $(''#camera_wrap'').camera({\r\n    //thumbnails: true\r\n    //alignment     : ''centerRight'',\r\n    autoAdvance     : true,\r\n    mobileAutoAdvance : true,\r\n    fx          : ''scrollRight'',\r\n    height: ''36%'',\r\n    hover: false,\r\n    loader: ''none'',\r\n    navigation: true,\r\n    navigationHover: true,\r\n    mobileNavHover: true,\r\n    playPause: false,\r\n    pauseOnClick: false,\r\n    pagination      : true,\r\n    time: 5000,\r\n    transPeriod: 1000,\r\n    minHeight: ''250px''\r\n  });\r\n\r\n\r\n}); //\r\n$(window).load(function() {\r\n  //\r\n\r\n\r\n\r\n}); //\r\n</script>', ''),
(3, 'Szolgáltatások', 'szolgaltatasok', '<div>\r\n	<h2>\r\n		Fő tev&eacute;kenys&eacute;g&uuml;nk:</h2>\r\n	<ul>\r\n		<li>\r\n			Rovarirt&aacute;s (<a href="csotanyirtas.html" title="Rendszeres csótányirtás">cs&oacute;t&aacute;nyirt&aacute;s</a>, <a href="hangyairtas.html" title="Házi és fáraó hangyairtás">hangyairt&aacute;s</a>, <a href="darazsirtas.html" title="Speciális darázsirtás">dar&aacute;zsirt&aacute;s</a>, <a href="agyipoloska.html" title="Ágyi poloska irtása">&aacute;gyi poloska</a>, bolhairt&aacute;s, stb.)</li>\r\n		<li>\r\n			R&aacute;gcs&aacute;l&oacute;irt&aacute;s (<a href="patkanyirtas.html" title="Patkányirtás szakértelemmel">patk&aacute;nyirt&aacute;s</a>, <a href="egerirtas.html" title="">eg&eacute;rirt&aacute;s</a>)</li>\r\n		<li>\r\n			Vegyszeres gyomirt&aacute;s</li>\r\n		<li>\r\n			IPC/IPM (integr&aacute;lt k&aacute;rtevőirt&aacute;s)</li>\r\n	</ul>\r\n	<h2>\r\n		V&aacute;llaljuk:</h2>\r\n	<ul>\r\n		<li>\r\n			Vend&eacute;gl&aacute;t&oacute; ipari &eacute;s k&ouml;z&eacute;tkeztet&eacute;si egys&eacute;gek</li>\r\n		<li>\r\n			&Uuml;zletek, &uuml;zemek, rakt&aacute;rak</li>\r\n		<li>\r\n			Lak&oacute;telepek, lak&oacute;h&aacute;zak</li>\r\n		<li>\r\n			Mag&aacute;nlak&aacute;sok</li>\r\n		<li>\r\n			Sz&aacute;llod&aacute;k, &uuml;d&uuml;lők, di&aacute;kotthonok, t&ouml;megsz&aacute;ll&aacute;sok</li>\r\n		<li>\r\n			Oktat&aacute;si int&eacute;zm&eacute;nyek</li>\r\n		<li>\r\n			Eg&eacute;szs&eacute;g&uuml;gyi int&eacute;zm&eacute;nyek</li>\r\n		<li>\r\n			Piacok v&aacute;s&aacute;rcsarnokok</li>\r\n		<li>\r\n			Szem&eacute;ttelepek</li>\r\n	</ul>\r\n	<p>\r\n		<strong>K&aacute;rtevő-mentes&iacute;t&eacute;s&eacute;t eseti megrendel&eacute;ssel, &eacute;s szerződ&eacute;ssel egyar&aacute;nt garanci&aacute;val, a legalacsonyabb &aacute;rakon. </strong></p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(4, 'Hangyairtás', 'hangyairtas', '<p>\r\n	A hangy&aacute;k t&aacute;rsas csal&aacute;dokba szerveződve, &uacute;n. bolyokban &eacute;lnek. Rajz&aacute;skor ijesztő lehet t&ouml;meges megjelen&eacute;s&uuml;k, egy-egy nagyobb boly ak&aacute;r t&ouml;bb milli&oacute; hangy&aacute;b&oacute;l is &aacute;llhat.</p>\r\n<h2>\r\n	Mikor sz&uuml;ks&eacute;ges a hangyairt&aacute;s?</h2>\r\n<p>\r\n	Elősz&ouml;r csak egy-k&eacute;t hangya bukkan fel a lak&aacute;sban/irod&aacute;ban. Azt&aacute;n m&aacute;r sz&eacute;p hossz&uacute; sorban menetelő hadoszlopot l&aacute;tunk. Majd megjelenik a teljes hangya hadsereg... A hangy&aacute;k szervezetten műk&ouml;dnek: elősz&ouml;r felder&iacute;tik a terepet, majd a beazonos&iacute;tott &eacute;lelemlelőhelyekre kik&uuml;ldik a begyűjtőket. S min&eacute;l t&ouml;bb &eacute;lelmet tal&aacute;lnak, ann&aacute;l t&ouml;bb hangya j&ouml;n, &eacute;s ann&aacute;l ink&aacute;bb elszaporodnak, &iacute;gy a hangyairt&aacute;s nem az a dolog, amit &eacute;rdemes halogatni.</p>\r\n<h2>\r\n	Mi&eacute;rt sz&uuml;ks&eacute;ges a hangyairt&aacute;s?</h2>\r\n<p>\r\n	A hangyairt&aacute;s &eacute;lelmiszerbiztons&aacute;gi, higi&eacute;niai okokb&oacute;l is fontos azon t&uacute;l, hogy ezek a rovarok az &eacute;lelmiszerekre, főleg &eacute;dess&eacute;gekre r&aacute;m&aacute;szva kellemetlenkednek. Az al&aacute;bbiakban a haz&aacute;nkban legelterjedtebb k&eacute;t fajt ismertetj&uuml;k, &eacute;s az eset&uuml;kben alkalmazott hangyairt&aacute;s m&oacute;dszereit.</p>\r\n<p>\r\n	<strong>H&aacute;zi hangya:</strong> Elsősorban kertes h&aacute;zakban, &aacute;ltal&aacute;ban tavasszal &eacute;s ősszel jelenik meg ez a 3-4 mm hossz&uacute;s&aacute;g&uacute;, v&ouml;r&ouml;sesbarna rovar, amely a sz&aacute;raz &eacute;s meleg helyeket kedveli. Mindenevő, a szabadban fők&eacute;nt a lev&eacute;ltetvek nedv&eacute;t fogyasztja. A h&aacute;zi hangya a f&ouml;ldben f&eacute;szkel, egy-egy kol&oacute;nia egy any&aacute;b&oacute;l &eacute;s t&ouml;bb ezer dolgoz&oacute;b&oacute;l &aacute;ll. Akt&iacute;van terjed; bev&aacute;ndorol a lak&aacute;sba &eacute;lelemszerz&eacute;s c&eacute;lj&aacute;b&oacute;l, de be is hurcolhat&oacute; (pl. egy cserepes n&ouml;v&eacute;nnyel).</p>\r\n<p>\r\n	<strong>Hangyairt&aacute;s h&aacute;zi hangya eset&eacute;n:</strong><br />\r\n	C&eacute;lzott s&aacute;vpermetez&eacute;st alkalmazunk.</p>\r\n<p>\r\n	<strong>F&aacute;ra&oacute;hangya:</strong> V&ouml;r&ouml;sesbarna, 2 mm hossz&uacute;s&aacute;g&uacute;, a meleg &eacute;g&ouml;vről sz&aacute;rmaz&oacute; rovar. Kol&oacute;ni&aacute;j&aacute;t &eacute;pp ez&eacute;rt az &eacute;p&uuml;letek fal&aacute;ban, &aacute;ltal&aacute;ban a fűtő- vagy melegv&iacute;z rendszerek k&ouml;zel&eacute;ben alak&iacute;tja ki. Rendk&iacute;v&uuml;l szapora faj; egy f&aacute;ra&oacute;hangya bolyban t&ouml;bb, olykor ak&aacute;r t&ouml;bb sz&aacute;z kir&aacute;lynő is &eacute;lhet egy&uuml;tt, amelyek feladata az ut&oacute;dok biztos&iacute;t&aacute;sa.</p>\r\n<h3>\r\n	Hangyairt&aacute;s f&aacute;ra&oacute;hangy&aacute;k eset&eacute;n:</h3>\r\n<p>\r\n	Speci&aacute;lis f&aacute;ra&oacute;hangya-irt&oacute; csal&eacute;tket alkalmazunk.</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(5, 'Patkányirtás', 'patkanyirtas', '\r\n	<p><strong>Vándorpatkány:</strong><br>\r\nTársas élőlény, kisebb-nagyobb hordákban él, éjszakai életmódot folytat. Nappal többnyire fészkében tartózkodik, amelyet épületek közelében, a földben alakít ki, de nagyvárosok csatornarendszereiben is megtelepedhet. Átlagosan 2-3 évig él, ivarérettségét rendkívül gyorsan, 3 hónaposan éri el.</p>\r\n\r\n	<p>Mindenevő, de ha módjában áll, akkor válogat; végigkóstol többféle élelmiszert, és a számára megfelelőt eszi meg. A dohos, penészes, romlott ételt nem fogyasztja el. Rosszul tűri a szomjúságot és az éhséget, szükség esetén akár beteg fajtársait és utódjait is felfalhatja.</p>\r\n\r\n	<h2>A patkányirtás fontossága</h2>\r\n\r\n	<p>A patkányirtás kiemelten fontos feladat. Egyrészt, ez a rágcsáló rendkívül szapora: évente akár több száz utódja is lehet egyetlen párnak! Másrészt, számos kórokozót, s ezáltal fertőzést hurcol, így higiéniai szempontból is lényeges a megfékezése.</p>\r\n\r\n	<h2>Patkányirtás, de hogyan?</h2>\r\n\r\n	<p>A patkányoknál nem alkalmazhatóak az egérirtásnál használt módszerek, csapdák, mivel a patkányok sokkal nagyobb testűek, és táplálkozási szokásaik is eltérnek az egerekétől.</p>\r\n\r\n    <p>A védekezés lehetőségei: A patkányirtás során mérgezett csalétek kihelyezésével, speciális patkányetető szerelvénnyel dolgozunk.</p>\r\n    \r\n    <h2>Patkányirtás korszerűen</h2>\r\n    \r\n    <p>Míg régebben a patkányirtás általában egyszerű patkányméreggel történt, napjainkban kifejezetten patkányirtásra kifejlesztett, speciális dobozcsapdák kihelyezésével végezzük. </p>\r\n        \r\n    <p>Ehhez nem férnek hozzá más állatok - kutya, macska, így a patkányirtás során nem fordulhat elő, hogy véletlenül a házi kedvenc kóstol bele a méregbe.</p>', 0, '', 0, 1, '', '', '', '', '', ''),
(6, 'Csótányirtás', 'csotanyirtas', '\r\n    <p>A csótányok rendkívül jól alkalmazkodnak a körülményekhez, így megfelelő életfeltételek mellett gyorsan elszaporodhatnak gyakorlatilag bárhol. Társasházi lakásokban általában a vizes blokkokban, ritkábban az előszobában fordulnak elő, illetve a lakóházak szemétledobóinak környékén és a pincékben, hőközpontoknál gyakoriak. </p>\r\n    \r\n    <p>Cégünk társasházi szerződés esetén a csótányirtást mind a közös helyiségekben, mind a lakásokban elvégzi a szerződésben foglalt gyakorisággal, de természetesen eseti megbízással is vállalunk csótánymentesítést bármilyen típusú ingatlannál. </p>\r\n    \r\n    <p><strong>Német csótány:</strong> Ez a 10-15 mm nagyságú, sárgásbarna színű rovar a legjobb alkalmazkodóképességű csótány, amely ennek köszönhetően a legelterjedtebb is Európában. A csótányirtás során ezzel a fajjal találkozunk leggyakrabban.</p>\r\n    \r\n    <p><strong>Konyhai csótány:</strong> A 20-27 mm nagyságú, feketésbarna színű konyhai csótány lassabban fejlődik ki, mint a német csótány, és szaporodási üteme is lassabb, valamint nagyobb a folyadékigénye.</p>\r\n    \r\n    <p><strong>Amerikai csótány:</strong> 35-40 mm nagyságú, vöröses-barna színű rovar. A három csótány faj közül ennek a leghosszabb a fejlődési ideje (150-450 nap). Az egyetlen faj, amely szárnyait repülésre használja. Magyarországon ritkán találkozunk ezzel a fajjal a csótányirtás során.</p>\r\n    \r\n    <p>A csótánymentesség biztosítása csakis folyamatos és körültekintő munkával, rendszeres csótányirtás révén lehetséges.</p>\r\n    \r\n    <h2>A csótányirtás lehetőségei</h2>\r\n    \r\n    <ul>\r\n    <li>Permetezés: ez a legismertebb eljárás, amely a legolcsóbb ugyan, de gyakrabban szükséges megismételni.</li>\r\n    <li>Gélezés: Forradalmian új és hosszabb távon ható módszer, amelyet a mi szakembereink is nagy sikerrel alkalmaznak.</li>\r\n    </ul>', 0, '', 0, 1, '', '', '', '', '', ''),
(7, 'Darázsirtás', 'darazsirtas', '<section>\r\n    \r\n	<p>Darázs: A darazsak társas kolóniákban élnek, egyéves családokban - kizárólag a nőstények telelnek át, s élnek hosszabb ideig. Egy államhoz egy királynő tartozik. A darázsfészek alakja és elhelyezkedése fajonként eltérő. </p>    \r\n    \r\n    <h2>Darázsirtás - házilag ne!</h2>\r\n    \r\n    <p>A darázsirtás elvégzését mindig bízzuk tapasztalt szakemberre az esetleges sérülések elkerülése érdekében! Ne próbálkozzunk vele házilagos módszerekkel. Az időben és szakszerűen elvégzett darázsirtás révén számos kellemetlenség elkerülhető. </p>\r\n    \r\n    <h2>Hol szükséges a darázsirtás?</h2>\r\n    \r\n    <p>Darazsakkal főként kertes övezetekben találkozhatunk, de előfordulnak panelházaknál is. Csípésük fájdalmas, az arra érzékeny, allergiás egyéneknél komoly ártalmakat is okozhat a fullánkjukkal bejuttatott méreganyag. Különösen veszélyes a szemen vagy nyakon ért darázscsípés. </p>\r\n    \r\n    <p>A magánlakások és családi házak mellett az óvodák, iskolák, kórházak és egyéb közintézmények, szállodák, nyaralóhelyek környékén rendszeres ellenőrzés szükséges (ez a lakó, intézményvezető, stb. feladata), mert a darázsirtás hiánya komoly problémákat okozhat.</p>\r\n    \r\n    <h2>Több darázsirtás volt már, de mindig visszatérnek a darazsak?</h2>\r\n    \r\n    <p>A darázsirtás során el kell pusztítani a teljes darázsfészket, és az összes rovart - különös tekintettel a királynőre. Ha nem pusztul el a királynő, akkor viszonylag rövid idő alatt újraépítik a kolóniát (arról nem is szólva, hogy a darázsirtás után igencsak dühösen reagálnak a környezetükben megjelenő emberekre).</p>\r\n    \r\n    <h3>Darázsirtás garantált eredménnyel</h3>\r\n    \r\n    <p>A darázsirtást végző szakember ismeri az adott darázsfaj jellemzőit, ki tudja választani a leghatékonyabb irtási módszert, és rendelkezik megfelelő védőruházattal, eszközökkel a feladathoz. </p>\r\n    \r\n    <p>Figyelem, ha lódarazsakkal gyűlt meg a baja, a darázsirtás mindenképp szakembert igényel! A lódarázs testhossza 34-35 mm, ez a darázsfaj életveszélyes lehet, ha a fészkét védi! Ilyen esetben semmiképp ne próbálkozzon házilagos darázsirtással, inkább hívjon bennünket!</p>\r\n    \r\n    <p>A darázsirtás során speciális irtószerrel kezeljük a darázsfészket, és lehetőség szerint teljesen eltávolítjuk azt.</p>', 0, '', 0, 1, '', '', '', '', '', ''),
(8, 'Ágyi poloska irtás', 'agyipoloskairtas', '<p>\r\n	Az &aacute;gyi poloska a f&eacute;nyker&uuml;lő, v&eacute;rsz&iacute;v&oacute; rovarok k&ouml;z&eacute; tartozik. Alakja ov&aacute;lis, 4-8 mm hossz&uacute;s&aacute;g&uacute;. Ez egy v&ouml;r&ouml;sesbarna &eacute;lősk&ouml;dő, tor&aacute;n bűzmirigy tal&aacute;lhat&oacute;, amiből folyamatos, kellemetlen szagot &aacute;raszt. &Eacute;jjel akt&iacute;v, a nappali időszakban pedig hasad&eacute;kokba, reped&eacute;sekbe, s&ouml;t&eacute;t &eacute;s huzatmentes helyekre h&uacute;z&oacute;dik be. V&aacute;ltozatos b&uacute;v&oacute;helyeket tal&aacute;l mag&aacute;nak, pl. k&eacute;pkeret, f&uuml;gg&ouml;nykarnis, csill&aacute;r felf&uuml;ggeszt&eacute;s, de legink&aacute;bb a k&aacute;rpitozott b&uacute;torok h&eacute;zagaiban &eacute;s &aacute;gyreped&eacute;sekben szeret rejtőzk&ouml;dni, ez&eacute;rt is kapta az &aacute;gyi poloska nevet.</p>\r\n<h2>\r\n	Ezek utalhatnak az &aacute;gyi poloska jelenl&eacute;t&eacute;re:</h2>\r\n<p>\r\n	Az &aacute;gyi poloska legink&aacute;bb rejtőzk&ouml;dő &eacute;letm&oacute;dot folytat, ez&eacute;rt igen neh&eacute;z &eacute;szrevenni - előfordul, hogy csak a f&aacute;j&oacute;, viszkető cs&iacute;p&eacute;snyomok &aacute;rulkodnak a jelenl&eacute;t&eacute;ről reggel. Ezek utalhatnak m&eacute;g a rovarfertőz&ouml;tts&eacute;gre:</p>\r\n<ul>\r\n	<li>\r\n		Mivel v&eacute;rrel t&aacute;pl&aacute;lkozik &eacute;s &aacute;lm&aacute;ban k&ouml;zel&iacute;ti meg az embert, az &aacute;gyi poloska jelenl&eacute;t&eacute;re utal&oacute; nyom lehet az &aacute;gyneműn tal&aacute;lhat&oacute; v&eacute;rfolt. Ugyanis amennyiben alv&aacute;s k&ouml;zben agyonnyomtuk a rovart &eacute;jszakai forgol&oacute;d&aacute;sunkkal, miut&aacute;n m&aacute;r telesz&iacute;vta mag&aacute;t v&eacute;r&uuml;nkkel, akkor kr&ouml;&uuml;lbel&uuml;l 1-2 cm-es v&eacute;rfolt maradhat a lepedőn.</li>\r\n	<li>\r\n		Szint&eacute;n nyom lehet az &aacute;gyi poloska &uuml;r&uuml;l&eacute;ke, amely homokszem m&eacute;retű, fekete sz&iacute;nű foltok form&aacute;j&aacute;ban l&aacute;that&oacute; az &aacute;gyon, illetve ha alapsoabban &aacute;tvizsg&aacute;ljuk a lehets&eacute;ges rejtekhelyeket, akkor a r&eacute;sekn&eacute;l, reped&eacute;sekn&eacute;l.</li>\r\n	<li>\r\n		Tov&aacute;bbi jel lehet a poloska levedlett bőre. Egy &aacute;gyi poloska a teljes fejlőd&eacute;si ciklusa sor&aacute;n 5-sz&ouml;r vedlik, ilyenkor az elhalt sz&ouml;vet &uuml;res burokk&eacute;nt marad h&aacute;tra.</li>\r\n</ul>\r\n<h2>\r\n	Az &aacute;gyi poloska szaporod&aacute;sa</h2>\r\n<p>\r\n	Sajnos, ez a kellemetlen rovar rendk&iacute;v&uuml;li gyorsas&aacute;ggal szaporodik, mire &eacute;szrevessz&uuml;k jelenl&eacute;t&eacute;t a lak&aacute;sban, m&aacute;r t&ouml;megesen megtelepedtek.<br />\r\n	Egy nőst&eacute;ny &aacute;gyi poloska napi 2-3 pet&eacute;t rak, vagyis teljes &eacute;letciklusa (ami kb. 1,5 &eacute;v) alatt nagyj&aacute;b&oacute;l &ouml;tsz&aacute;zat. A feh&eacute;res sz&iacute;nű pet&eacute;k megk&ouml;zel&iacute;tőleg 1 mm hossz&uacute;s&aacute;g&uacute;ak, a kifejlőd&eacute;s&uuml;kh&ouml;z sz&uuml;ks&eacute;ges idő optim&aacute;lis k&ouml;r&uuml;lm&eacute;nyek k&ouml;z&ouml;tt 6-10 nap (kedvezőtlenebb k&ouml;r&uuml;lm&eacute;nyek k&ouml;z&ouml;tt ak&aacute;r 30 nap is lehet). A lak&oacute;terek p&aacute;ratartalma &eacute;s hőm&eacute;rs&eacute;klete ide&aacute;lis felt&eacute;teleket jelent a rovarok sz&aacute;m&aacute;ra.</p>\r\n<h2>\r\n	Hogyan ker&uuml;lt &aacute;gyi poloska a lak&aacute;somba?</h2>\r\n<p>\r\n	Ha valaki net&aacute;n &uacute;gy gondoln&aacute;, hogy &aacute;gyi poloska egy tiszta, rendben tartott lak&aacute;sban nem fordulhat elő, az nagyot t&eacute;ved. Ezt a rovart nem a kosz vonzza, hanem az ember, pontosabban az emberi v&eacute;r.</p>\r\n<p>\r\n	&Aacute;gyi poloska k&eacute;t m&oacute;don ker&uuml;lhet a lak&aacute;sba: bem&aacute;szik valahonnan a saj&aacute;t l&aacute;b&aacute;n, vagy pedig m&aacute;sok hozz&aacute;k be. Ut&oacute;bbi esetnek is t&ouml;bb fajt&aacute;ja van:</p>\r\n<ul>\r\n	<li>\r\n		Egy nyaral&aacute;s sor&aacute;n a sz&aacute;llod&aacute;ban, hotelben k&ouml;nnyen &ouml;ssze lehet szedni ezt a rovart, hiszen az egym&aacute;st v&aacute;lt&oacute; vend&eacute;gek hozz&aacute;k-viszik magukkal az apr&oacute; kis v&eacute;rsz&iacute;v&oacute;kat. A csomagokkal, bőr&ouml;nd&ouml;kkel pedig egyszerűen eljutnak a lak&aacute;sig.</li>\r\n	<li>\r\n		Haszn&aacute;lt b&uacute;tor v&aacute;s&aacute;rl&aacute;sakor nagyon &oacute;vatosnak kell lenni, hiszen nem tudhat&oacute;, hogy a kor&aacute;bbi helyen volt-e &aacute;gyi poloska fertőz&ouml;tts&eacute;g.</li>\r\n	<li>\r\n		&Uacute;j b&uacute;tor v&aacute;s&aacute;rl&aacute;sakor sem vagyunk biztons&aacute;gban: a b&uacute;torgy&aacute;rban, a rakt&aacute;rban, a boltban vagy a sz&aacute;ll&iacute;t&oacute; j&aacute;rműben (ahol esetleg haszn&aacute;lt b&uacute;torok is megfordultak) előfordulhatnak a nem k&iacute;v&aacute;natos potyautasok, b&aacute;r kicsi az es&eacute;lye.</li>\r\n</ul>\r\n<p>\r\n	Mindenesetre, a &uacute;jonnan beker&uuml;lt b&uacute;torokat &eacute;rdemes alaposan kiporsz&iacute;v&oacute;zni, &aacute;tt&ouml;r&ouml;lni, csomagol&oacute;anyagaikat &aacute;tn&eacute;zni &eacute;s azonnal kidobni.</p>\r\n<h2>\r\n	Ha megt&ouml;rt&eacute;nt a baj...</h2>\r\n<p>\r\n	Amennyiben valamilyen m&oacute;don &aacute;gyi poloska ker&uuml;lt otthon&aacute;ba, mindenk&eacute;pp h&iacute;vjon szakembert. Ennek a rovarnak a h&aacute;zilagos rovarirt&oacute; szerekkel t&ouml;rt&eacute;nő kiirt&aacute;sa gyakorlatilag lehetetlen.</p>\r\n<p>\r\n	<strong>Az egyetlen hat&eacute;kony v&eacute;dekez&eacute;s, ha a fertőz&ouml;tt ter&uuml;letet permetez&eacute;ssel kezelj&uuml;k, majd speci&aacute;lis rovarirt&oacute;/kiűző f&uuml;stpatron seg&iacute;ts&eacute;g&eacute;vel kiűzz&uuml;k a rovarokat a b&uacute;v&oacute;hely&uuml;kről.</strong></p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(9, 'Rágcsálóirtás', 'ragcsaloirtas', ' <p>Magyarországon a rágcsálóirtás főként a patkány- és az egérirtás területét érinti, mivel ez a két rágcsáló fajta a leginkább elterjedt hazánkban. Nagyon nagy károkat tudnak okozni bármilyen épületben, ugyanis kifejezetten mohó és szapora állatok, továbbá fertőzésveszélyt is jelentenek. <br>\r\n    <strong>Az egészségügyi rendeletek a gyártókat, illetve az élelmiszerforgalmazókat is kötelezik a rendszeres rágcsálóirtás elvégzésére.</strong></p>\r\n    \r\n    <h2>Miért van szükség a rágcsálóirtásra?</h2>\r\n    \r\n    <p>A rágcsálóirtás szükségessége megkérdőjelezhetetlen: a rágcsálók komoly fertőzéseket terjesztenek, elszaporodásuk esetén példátlanul nagy szerepet játszhatnak a haszonállatokra, valamint az emberre is átterjedő járványok kialakulásában. A gyárak, szállodák, raktárak mellett a családi házak és a társasházak sincsenek biztonságban tőlük; bárhová gond nélkül bejutnak ezek a fürge kis állatok. Ebből adódóan egészségügyi és gazdasági szempontból is egyaránt fontos a rágcsálóirtás azonnali kivitelezése.</p>\r\n    \r\n    <h2>Rágcsálóirtás, de mikor?</h2>\r\n    \r\n    <p>Mivel a rágcsálók kifejezetten gyorsan tudnak szaporodni, a dolgot nem ajánlott halogatni, a rágcsálóirtási feladatokat minél előbb el kell végezni, amennyiben felfedeztük az egerek, patkányok nyomait. Számos borzalmas példa van rá világszerte, hogy ahol a rágcsálóirtás feladatait nem kezdték meg időben, és csak a megnövekedett patkány vagy egér populáció után kezdtek el cselekedni, ott nagyon nehéz, esetenként lehetetlen volt megállítani a rágcsálóinváziót.</p>\r\n    \r\n    <h2>Rágcsálóirtás = egérirtás?</h2>\r\n    \r\n    <p>Akár mezőgazdasági, akár lakossági oldalról nézzük, a leggyakrabban igényelt rágcsálóirtási szolgáltatásunk kétségkívül az egérirtás. Többnyire a megrendelők a rágcsálóirtás szót szinonimaként használják az egérirtásra, azonban a rágcsálók körébe igen sok más állatfaj is tartozik, amelyek hatalmas mennyiségű károkat okozhatnak a terményben, élelmiszerekben (patkány, pocok, stb.).</p>\r\n    \r\n    <p>Vannak, akik a rágcsálóirtás alá sorolják a különféle kisebb ragadozók (görény, nyest, stb.) problémakörét is, pedig ezen állatok nem tartoznak az egészségügyi károkozók közé állattani szempontból, és az ellenük való védekezés is másféle módszereket igényel - védett állatok, éppezért élvebefogóval szabad csak eltávolítani őket egy adott területről!</p>\r\n    \r\n    <p><strong>Természetesen cégünk minden téren készséggel áll az Önök rendelkezésére a rágcsálóirtás vagy ahhoz kapcsolódó problémák megoldásában.</strong></p>\r\n    \r\n    <h3>Professzionális rágcsálóirtás</h3>\r\n    \r\n    <p>A szakember kifejezetten rágcsálóirtásra kifejlesztett mérgezett csalétkekkel dolgozik, viszont ennél sokkal lényegesebb, hogy ismeri a kártevők szokásait, életmódjait, így  könnyen fel tudja deríteni és 100 százalékosan felszámolni a fészket is. A profi rágcsálóirtás során korszerű mérgeket és eszközöket alkalmazunk, így az eredmény garantált.</p>', 0, '', 0, 1, '', '', '', '', '', ''),
(10, 'Rovarirtás', 'rovarirtas', '<p>\r\n	A rovarirt&aacute;s haz&aacute;nkban elsősorban a cs&oacute;t&aacute;ny, a hangya, az &aacute;gyi poloska &eacute;s dar&aacute;zsirt&aacute;s tev&eacute;kenys&eacute;g&eacute;re korl&aacute;toz&oacute;dik. A forradalmian &uacute;j g&eacute;lez&eacute;si technol&oacute;gia seg&iacute;ts&eacute;g&eacute;vel a cs&oacute;t&aacute;nyirt&aacute;s ma m&aacute;r nem ig&eacute;nyel speci&aacute;lis elők&eacute;sz&uuml;letet. Teljesen szagtalan a művelet, nincsenek l&aacute;that&oacute; nyomok, nem kell a rovarirt&aacute;s ut&aacute;n takar&iacute;tani. Eredm&eacute;nyess&eacute;ge garant&aacute;ltan hat&eacute;kony. Tapasztalt rovarirt&oacute; szakember&uuml;nk a legkorszerűbb &eacute;s a leghat&eacute;konyabb rovarirt&oacute;szereket alkalmazza, melyek az Eur&oacute;pai Uni&oacute;ban regisztr&aacute;ltak &eacute;s ellenőrz&ouml;tt minős&eacute;gűek. A megfelelő hat&oacute;anyagokkal v&eacute;gzett prec&iacute;z rovarirt&aacute;s mindenki sz&aacute;m&aacute;ra eredm&eacute;nyes, m&eacute;gis biztons&aacute;gos. A korszerű technol&oacute;gi&aacute;val &eacute;s szak&eacute;rtelemmel teljes m&eacute;rt&eacute;kben garant&aacute;ljuk az eredm&eacute;nyes munkav&eacute;gz&eacute;st &Ouml;nnek!</p>\r\n<p>\r\n	<strong><a href="csotanyirtas">Cs&oacute;t&aacute;nyirt&aacute;s</a> | <a href="hangyairtas">Hangyairt&aacute;s</a> | <a href="darazsirtas">Dar&aacute;zsirt&aacute;s</a> | <a href="agyipoloskairtas">&Aacute;gyi poloska</a></strong></p>\r\n<p>\r\n	<strong>A bolha:</strong> A bolh&aacute;k, mint a v&eacute;rsz&iacute;v&oacute;k &aacute;ltal&aacute;ban, rejtett &eacute;letm&oacute;dot folytatnak.<br />\r\n	&Eacute;p&uuml;leteinkben robban&aacute;sszerű elszaporod&aacute;sra is k&eacute;pesek. Az &aacute;rtalom megelőz&eacute;se &eacute;rdek&eacute;ben fontos a tisztas&aacute;g, valamint az &aacute;lland&oacute; takar&iacute;t&aacute;s. A poros parketta, a h&eacute;zagos kialak&iacute;t&aacute;s&uacute; padl&oacute;zat, valamint a padl&oacute;szőnyeg ide&aacute;lis k&ouml;r&uuml;lm&eacute;nyeket biztos&iacute;t a bolha szaporod&aacute;s&aacute;hoz. A helys&eacute;gek rendszeres takar&iacute;t&aacute;sa a bolhal&aacute;rv&aacute;k fejlőd&eacute;s&eacute;t akad&aacute;lyozza meg. Tov&aacute;bbi lehetős&eacute;g, a padozat forr&oacute; v&iacute;zzel t&ouml;rt&eacute;nő fels&uacute;rol&aacute;sa. A bolha&aacute;rtalom időleges kiv&eacute;d&eacute;s&eacute;re sz&uacute;nyogok &eacute;s kullancsok t&aacute;voltart&aacute;s&aacute;ra haszn&aacute;lt bőrfel&uuml;let kezel&eacute;s&eacute;re haszn&aacute;lt szerek is hat&eacute;konyak, azonban a ruh&aacute;zat v&eacute;delme ilyen esetben sem megoldott. V&eacute;gleges &eacute;s tart&oacute;s megold&aacute;s a szakember &aacute;ltal t&ouml;rt&eacute;nő permetez&eacute;s. A fel&uuml;leti kezel&eacute;s kombin&aacute;lt hat&oacute;anyag&uacute; bolha irt&oacute;szerrel t&ouml;rt&eacute;nik, mely az im&aacute;g&oacute;k (kifejlett p&eacute;ld&aacute;nyok) elpuszt&iacute;t&aacute;s&aacute;n k&iacute;v&uuml;l a l&aacute;rv&aacute;k el&ouml;l&eacute;s&eacute;t is biztos&iacute;tja. Zs&uacute;folts&aacute;g &eacute;s nagy fertőz&ouml;tts&eacute;g eset&eacute;n f&uuml;stk&eacute;pz&eacute;s alkalmaz&aacute;sa j&ouml;het sz&oacute;ba.</p>\r\n<p>\r\n	<strong>K&ouml;z&ouml;ns&eacute;ges kullancs:</strong> Lapos toj&aacute;sdad alak&uacute; 1-4 mm nagys&aacute;g&uacute; s&aacute;rg&aacute;sbarna sz&iacute;nű v&eacute;rsz&iacute;v&oacute; rovar. Erdőkben bokros boz&oacute;tos nyirkos helyeken &eacute;l. Az aljn&ouml;v&eacute;nyzeten megkapaszkodva v&aacute;rja a v&eacute;rt ad&oacute; gazda felbukkan&aacute;s&aacute;t, &eacute;s onnan m&aacute;szik r&aacute; &aacute;ldozat&aacute;ra. Az emberre kapaszkodott kullancs a ruh&aacute;n vagy bőr&ouml;n, tov&aacute;bbm&aacute;szik &eacute;s a megfelelő helyen megtelepszik, &eacute;s csak n&eacute;h&aacute;ny &oacute;ra ut&aacute;n sz&iacute;v v&eacute;rt. A kullancsok vesz&eacute;lyes betegs&eacute;geket terjesztenek, (pl: lyme k&oacute;r, agyveőgyullad&aacute;s). V&eacute;dekez&eacute;s: Fertőz&ouml;tt ter&uuml;letet 1.5 m&eacute;ter magass&aacute;gig irt&oacute;szerrel kezelj&uuml;k.</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(11, 'Gyomirtás', 'gyomirtas', '<h2>\r\n	Vegyszeres gyomirt&aacute;st v&aacute;llalunk:</h2>\r\n<ul>\r\n	<li>\r\n		vesz&eacute;lyes (allergi&aacute;t okoz&oacute;) gyomok irt&aacute;sa (pl. parlagfű, fekete &uuml;r&ouml;m)</li>\r\n	<li>\r\n		művel&eacute;sen k&iacute;v&uuml;li ter&uuml;leteken</li>\r\n	<li>\r\n		h&aacute;ztetők (lapostetők) fel&uuml;let&eacute;n</li>\r\n	<li>\r\n		j&aacute;r&oacute;lapok k&ouml;z&ouml;tt</li>\r\n	<li>\r\n		egy&eacute;b nehezen gyommentesen tarthat&oacute; ter&uuml;leteken, stb.</li>\r\n</ul>\r\n<p>\r\n	N&ouml;v&eacute;nyv&eacute;delmi szolg&aacute;ltat&aacute;sunk sor&aacute;n egyar&aacute;nt fontos c&eacute;l a hat&eacute;konys&aacute;g, a takar&eacute;koss&aacute;g &eacute;s a k&ouml;rnyezet v&eacute;delme. A term&eacute;szetben leboml&oacute; gyomirt&oacute;szereket haszn&aacute;lunk &eacute;s magas sz&iacute;nvonal&uacute; technol&oacute;gi&aacute;nknak k&ouml;sz&ouml;nhetően csak a sz&uuml;ks&eacute;ges helyekre &eacute;s csak sz&uuml;ks&eacute;ges mennyis&eacute;gben juttatunk ki vegyszert. V&aacute;lassz&aacute;k &Ouml;n&ouml;k is takar&eacute;kos, hat&eacute;kony, k&ouml;rnyezetk&iacute;m&eacute;lő szolg&aacute;ltat&aacute;sunkat!</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(12, 'Referenciák', 'referenciak', '<h2>\r\n	Tekintse meg referenci&aacute;inkat!</h2>\r\n<ul>\r\n	<li>\r\n		&Eacute;ttermek</li>\r\n	<li>\r\n		P&eacute;ks&eacute;gek</li>\r\n	<li>\r\n		T&aacute;rsash&aacute;zak</li>\r\n	<li>\r\n		Sz&aacute;llod&aacute;k-Sz&aacute;ll&oacute;k</li>\r\n	<li>\r\n		Iskol&aacute;k</li>\r\n	<li>\r\n		Casinok</li>\r\n	<li>\r\n		Benzinkutak</li>\r\n	<li>\r\n		Party service</li>\r\n	<li>\r\n		Ipari parkok</li>\r\n	<li>\r\n		&Eacute;lelmiszer forgalmaz&oacute;k elő&aacute;ll&iacute;t&oacute;k</li>\r\n	<li>\r\n		Borforgalmaz&oacute;k</li>\r\n	<li>\r\n		Cukr&aacute;szd&aacute;k</li>\r\n</ul>\r\n<p>\r\n	<strong>Bővebb, r&eacute;szletes referencialist&aacute;t aj&aacute;nlatunkhoz tudunk mell&eacute;kelni!</strong></p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(13, 'Elérhetőségünk', 'elerhetoseg', '<h2>\r\n	Irt&oacute;j&oacute;irt&aacute;s Kft.</h2>\r\n<p>\r\n	1188 Budapest Bocskai utca 103/B<br />\r\n	Tel/fax: 06 1/630-47-20</p>\r\n<p>\r\n	<strong>&Uuml;gyvezető: </strong>T&ouml;r&ouml;k B&eacute;la<br />\r\n	06 20/587-89-48</p>\r\n<p>\r\n	<strong>Titk&aacute;rs&aacute;g:</strong> Bogn&aacute;r D&oacute;ra<br />\r\n	06 20/288-81-45</p>\r\n<p>\r\n	<strong>Titk&aacute;rs&aacute;gi asszisztens:</strong> T&ouml;r&ouml;kn&eacute; Vajda Szimonetta<br />\r\n	06-70/388-15-50</p>\r\n<p>\r\n	<strong>E-mail:</strong> info@irtojoirtas.hu<br />\r\n	<strong>Skype:</strong> irtojoirtas</p>\r\n<p>\r\n	Ad&oacute;sz&aacute;m: 14485297-2-43<br />\r\n	C&eacute;get bejegyző b&iacute;r&oacute;s&aacute;g neve: Főv&aacute;rosi T&ouml;rv&eacute;nysz&eacute;ki C&eacute;gb&iacute;r&oacute;s&aacute;ga<br />\r\n	C&eacute;gjegyz&eacute;ksz&aacute;m: 01-09-905924</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(14, '404', '404', '<div class="page-404">\r\n          <p class="txt1">404</p>\r\n          <p class="txt2">Oldal nem találva!</p>\r\n </div>', 0, '', 0, 1, '', '', '', '', '', ''),
(15, 'Cégünkről', 'cegunkrol', '<div>\r\n	<h2>\r\n		C&eacute;g&uuml;nkről</h2>\r\n	<h4>\r\n		Az Irtoj&oacute;irt&aacute;s Kft., fő tev&eacute;kenys&eacute;gi k&ouml;re a k&aacute;rtevő-mentes&iacute;t&eacute;s, de nemcsak az eg&eacute;szs&eacute;g&uuml;gyi k&aacute;rtevőkkel szembeni v&eacute;dekez&eacute;sben vagyunk &eacute;rdekeltek, hanem hat&eacute;konyan v&eacute;gezz&uuml;k el a gyomirt&aacute;si feladatokat is. N&ouml;v&eacute;nyv&eacute;dő szakm&eacute;rn&ouml;k&uuml;nk ir&aacute;ny&iacute;t&aacute;sa mellett, nagy sikerrel v&eacute;gezz&uuml;k ipari parkok, lapos tetős &eacute;p&uuml;letek, &eacute;s egy&eacute;b kisebb, nagyobb, ter&uuml;letek vegyszeres gyomtalan&iacute;t&aacute;s&aacute;t.</h4>\r\n	<p>\r\n		Munk&aacute;nk sor&aacute;n a megrendelőinkkel hossz&uacute; t&aacute;v&uacute; kapcsolatra t&ouml;reksz&uuml;nk, &eacute;s mindent elk&ouml;vet&uuml;nk annak &eacute;rdek&eacute;ben, hogy ezt &uuml;gyfeleink is &iacute;gy &eacute;rezz&eacute;k. Koll&eacute;g&aacute;inkat a prec&iacute;z munkav&eacute;gz&eacute;s mellett, &aacute;polt megjelen&eacute;s, &eacute;s kedves mindenre k&eacute;rd&eacute;sre kiterjedő t&aacute;j&eacute;koztat&aacute;s, jellemzi.</p>\r\n	<p>\r\n		Az eg&eacute;szs&eacute;g&uuml;gyi k&aacute;rtevőirt&aacute;ssal foglalkoz&oacute; szakembereink folyamatos tov&aacute;bbk&eacute;pz&eacute;seken vesznek r&eacute;szt, &iacute;gy t&aacute;j&eacute;kozottak a leg&uacute;jabb technol&oacute;gi&aacute;k, elj&aacute;r&aacute;sok, m&oacute;dszerek tekintet&eacute;ben, ami garancia a ny&uacute;jtott szolg&aacute;ltat&aacute;saink minős&eacute;g&eacute;re.</p>\r\n	<p>\r\n		A hat&aacute;lyos elő&iacute;r&aacute;sok, rendeletek, jogszab&aacute;lyok &aacute;ltal meghat&aacute;rozottak szerint v&eacute;gezz&uuml;k munk&aacute;nkat. A HACCP rendszerben műk&ouml;dő v&aacute;llalkoz&aacute;sok rovar &eacute;s r&aacute;gcs&aacute;l&oacute;irt&aacute;s&aacute;t az &eacute;ppen hat&aacute;lyos elő&iacute;r&aacute;soknak megfelelően dokument&aacute;ljuk, biztons&aacute;gtechnikai adatlapokat adunk a felhaszn&aacute;lt irt&oacute;szerekről, t&eacute;rk&eacute;pet k&eacute;sz&iacute;t&uuml;nk a r&aacute;gcs&aacute;l&oacute;irt&oacute; szerelv&eacute;nyek pontos hely&eacute;ről, &eacute;s fogy&aacute;si napl&oacute;t t&ouml;lt&uuml;nk ki, amin a k&eacute;sőbbiekben jel&ouml;lni tudjuk az esetleges r&aacute;gcs&aacute;l&oacute;irt&oacute;szer fogy&aacute;st.</p>\r\n	<p>\r\n		A mi seg&iacute;ts&eacute;g&uuml;nkkel k&ouml;nnyed&eacute;n &eacute;s kedvező &aacute;ron &eacute;rheti el, a legkorszerűbb (IPC/IPM) integr&aacute;lt k&aacute;rtevőirt&aacute;s. Ezek ut&aacute;n &Ouml;n nyugodt lehet, a k&aacute;rtevőirt&aacute;s tekintet&eacute;ben nem fognak hib&aacute;t tal&aacute;lni &Ouml;nn&eacute;l a k&ouml;zeg&eacute;szs&eacute;g&uuml;gyi ellenőr&ouml;k, ezt garant&aacute;ljuk &eacute;s k&aacute;rtevőmentes k&ouml;rnyezetet.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '', 0, 1, '', '', '', '', '', ''),
(16, 'Galéria', 'galeria', '', 0, '', 0, 1, '', '', '', '', '<script>\r\n$(document).ready(function() {\r\n  //\r\n\r\n  // touchTouch\r\n  $(''.thumb-isotope .thumbnail a'').touchTouch();\r\n\r\n\r\n\r\n\r\n\r\n\r\n}); //\r\n$(window).load(function() {\r\n  //\r\n\r\n  /*----------------------------------------------------*/\r\n  // ISOTOPE BEGIN\r\n  /*----------------------------------------------------*/\r\n  var $container = $(''#container'');\r\n  //Run to initialise column sizes\r\n  updateSize();\r\n\r\n  //Load fitRows when images all loaded\r\n  $container.imagesLoaded( function(){\r\n\r\n      $container.isotope({\r\n          // options\r\n          itemSelector : ''.element'',\r\n          layoutMode : ''fitRows'',\r\n          transformsEnabled: true,\r\n          columnWidth: function( containerWidth ) {\r\n              containerWidth = $browserWidth;\r\n              return Math.floor(containerWidth / $cols);\r\n            }\r\n      });\r\n  });\r\n\r\n  // update columnWidth on window resize\r\n  $(window).smartresize(function(){\r\n      updateSize();\r\n      $container.isotope( ''reLayout'' );\r\n  });\r\n\r\n  //Set item size\r\n  function updateSize() {\r\n      $browserWidth = $container.width();\r\n      $cols = 4;\r\n\r\n      if ($browserWidth >= 1170) {\r\n          $cols = 4;\r\n      }\r\n      else if ($browserWidth >= 767 && $browserWidth < 1170) {\r\n          $cols = 3;\r\n      }\r\n      else if ($browserWidth >= 480 && $browserWidth < 767) {\r\n          $cols = 2;\r\n      }\r\n      else if ($browserWidth >= 0 && $browserWidth < 480) {\r\n          $cols = 1;\r\n      }\r\n      //console.log("Browser width is:" + $browserWidth);\r\n      //console.log("Cols is:" + $cols);\r\n\r\n      // $gutterTotal = $cols * 20;\r\n      $browserWidth = $browserWidth; // - $gutterTotal;\r\n      $itemWidth = $browserWidth / $cols;\r\n      $itemWidth = Math.floor($itemWidth);\r\n\r\n      $(".element").each(function(index){\r\n          $(this).css({"width":$itemWidth+"px"});\r\n      });\r\n\r\n\r\n\r\n    var $optionSets = $(''#options .option-set''),\r\n        $optionLinks = $optionSets.find(''a'');\r\n\r\n    $optionLinks.click(function(){\r\n      var $this = $(this);\r\n      // don''t proceed if already selected\r\n      if ( $this.hasClass(''selected'') ) {\r\n        return false;\r\n      }\r\n      var $optionSet = $this.parents(''.option-set'');\r\n      $optionSet.find(''.selected'').removeClass(''selected'');\r\n      $this.addClass(''selected'');\r\n\r\n      // make option object dynamically, i.e. { filter: ''.my-filter-class'' }\r\n      var options = {},\r\n          key = $optionSet.attr(''data-option-key''),\r\n          value = $this.attr(''data-option-value'');\r\n      // parse ''false'' as false boolean\r\n      value = value === ''false'' ? false : value;\r\n      options[ key ] = value;\r\n      if ( key === ''layoutMode'' && typeof changeLayoutMode === ''function'' ) {\r\n        // changes in layout modes need extra logic\r\n        changeLayoutMode( $this, options )\r\n      } else {\r\n        // otherwise, apply new options\r\n        $container.isotope( options );\r\n      }\r\n\r\n      return false;\r\n    });\r\n\r\n  };\r\n  /*----------------------------------------------------*/\r\n  // ISOTOPE END\r\n  /*----------------------------------------------------*/\r\n\r\n\r\n\r\n}); //\r\n</script>', '');

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `slider`
--

CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `slider`
--

INSERT INTO `slider` (`id`, `sorrend`, `nev`, `file`, `nyelv`, `url`, `statusz`) VALUES
(1, 1, '', 'f0ed6-slide01.jpg', 'hu', '', 1),
(2, 2, '', 'c27a6-slide02.jpg', 'hu', '', 1),
(3, 3, '', '93b1d-slide03.jpg', 'hu', '', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termekek`
--

CREATE TABLE IF NOT EXISTS `termekek` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `kategoria` int(11) NOT NULL,
  `lead` text COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `fokep` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `keywords` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `description` varchar(500) COLLATE utf8_hungarian_ci NOT NULL,
  `header_custom_code` text COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL,
  `statusz` int(1) NOT NULL,
  `gyarto` int(11) NOT NULL,
  `marka` int(11) NOT NULL,
  `kiemelt` int(11) NOT NULL,
  `ar` int(11) NOT NULL,
  `ar_akcios` int(11) NOT NULL,
  `valuta` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `ar_beszerzesi` int(11) NOT NULL,
  `suly` int(11) NOT NULL,
  `raktarkeszlet` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_kepek`
--

CREATE TABLE IF NOT EXISTS `termek_kepek` (
  `id` int(11) NOT NULL,
  `title` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `termek` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `termek_tulajdonsagok`
--

CREATE TABLE IF NOT EXISTS `termek_tulajdonsagok` (
  `id` int(11) NOT NULL,
  `termek` int(11) NOT NULL,
  `tulajdonsag_id` int(11) NOT NULL,
  `tulajdonsag` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `url` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `szulo` int(200) NOT NULL,
  `nyelv` varchar(5) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `tulajdonsag_kat`
--

CREATE TABLE IF NOT EXISTS `tulajdonsag_kat` (
  `id` int(11) NOT NULL,
  `nev` varchar(256) COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(256) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_hungarian_ci NOT NULL,
  `perm` varchar(128) COLLATE utf8_hungarian_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- A tábla adatainak kiíratása `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `perm`) VALUES
(2, 'admin', 'c93ccd78b2076528346216b3b2f701e6', '1');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `beallitasok`
--
ALTER TABLE `beallitasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `companytype`
--
ALTER TABLE `companytype`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `dokumentumok`
--
ALTER TABLE `dokumentumok`
  ADD PRIMARY KEY (`dokumentumokid`);

--
-- A tábla indexei `galeria`
--
ALTER TABLE `galeria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyarto`
--
ALTER TABLE `gyarto`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `gyik`
--
ALTER TABLE `gyik`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek`
--
ALTER TABLE `hirek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kategoria`
--
ALTER TABLE `kategoria`
  ADD PRIMARY KEY (`kategoriaid`);

--
-- A tábla indexei `kategoriak`
--
ALTER TABLE `kategoriak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `marka`
--
ALTER TABLE `marka`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `megjelenes`
--
ALTER TABLE `megjelenes`
  ADD PRIMARY KEY (`megjelenesid`);

--
-- A tábla indexei `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `oldalak`
--
ALTER TABLE `oldalak`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termekek`
--
ALTER TABLE `termekek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_kepek`
--
ALTER TABLE `termek_kepek`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `beallitasok`
--
ALTER TABLE `beallitasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT a táblához `companytype`
--
ALTER TABLE `companytype`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT a táblához `dokumentumok`
--
ALTER TABLE `dokumentumok`
  MODIFY `dokumentumokid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `galeria`
--
ALTER TABLE `galeria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT a táblához `gyarto`
--
ALTER TABLE `gyarto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `gyik`
--
ALTER TABLE `gyik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek`
--
ALTER TABLE `hirek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `hirek_kategoria`
--
ALTER TABLE `hirek_kategoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoria`
--
ALTER TABLE `kategoria`
  MODIFY `kategoriaid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kategoriak`
--
ALTER TABLE `kategoriak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `kerdezzfelelek`
--
ALTER TABLE `kerdezzfelelek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `marka`
--
ALTER TABLE `marka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `megjelenes`
--
ALTER TABLE `megjelenes`
  MODIFY `megjelenesid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `nyelvi_forditasok`
--
ALTER TABLE `nyelvi_forditasok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `oldalak`
--
ALTER TABLE `oldalak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT a táblához `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT a táblához `termekek`
--
ALTER TABLE `termekek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_kepek`
--
ALTER TABLE `termek_kepek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `termek_tulajdonsagok`
--
ALTER TABLE `termek_tulajdonsagok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag`
--
ALTER TABLE `tulajdonsag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `tulajdonsag_kat`
--
ALTER TABLE `tulajdonsag_kat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT a táblához `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
